﻿using RpvStartBotService.Models;
using RpvStartBotService.Settings;
using System.IO;
using System.Linq;

namespace RpvStartBotService
{
    public static class Extensions
    {
        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {            
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }
            
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }
            
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        public static void UpdateTelegramSettings(this TelegramClientJson tlgJson, InputOptionsModel input)
        {
            tlgJson.BotSettings.BaseApiAddress = input.BaseApiAddress;
            for(int i = 0; i < tlgJson.BotSettings.Users.Count; i++)
            {
                tlgJson.BotSettings.Users[i].UserLogin = input.UserLogin;
                tlgJson.BotSettings.Users[i].UserPassword = input.UserPassword;               
                tlgJson.BotSettings.Users[i].ClientName = input.Tenant;

                if(input.ClientTelegramToken[i] != null)
                    tlgJson.BotSettings.Users[i].TelegramToken = input.ClientTelegramToken[i];                
            }           
           
        }
        public static void UpdateTelegramSettings(this TelegramAdminJson tlgJson, InputOptionsModel input)
        {
            tlgJson.BotSettings.BaseApiAddress = input.BaseApiAddress;
            for (int i = 0; i < tlgJson.BotSettings.Users.Count; i++)
            {
                tlgJson.BotSettings.Users[i].UserLogin = input.UserLogin;
                tlgJson.BotSettings.Users[i].UserPassword = input.UserPassword;
                tlgJson.BotSettings.Users[i].ClientName = input.Tenant;

                if (input.TelegramToken[i] != null)
                    tlgJson.BotSettings.Users[i].TelegramToken = input.TelegramToken[i];

                if (input.TelegramAdminToken[i] != null)
                    tlgJson.BotSettings.Users[i].TelegramAdminToken = input.TelegramAdminToken[i];
            }
            
        }

        public static void UpdateVkSettings(this VkJson vkJson, InputOptionsModel input)
        {            
            for(int i = 0; i< vkJson.Users.Count(); i++)
            {
                vkJson.Users[i].BaseUrl = input.BaseApiAddress;
                vkJson.Users[i].UserName = input.UserLogin;
                vkJson.Users[i].Password = input.UserPassword;
                vkJson.Users[i].TenacyName = input.Tenant;

                if(input.VkToken[i] != null)
                    vkJson.Users[i].Token = input.VkToken[i];

                if(input.GroupID[i] != 0)
                    vkJson.Users[i].GroupId = input.GroupID[i];

                if(!string.IsNullOrWhiteSpace(input.Currency[i]))
                    vkJson.Users[i].Currency = input.Currency[i];
            }
        }
    }
}
