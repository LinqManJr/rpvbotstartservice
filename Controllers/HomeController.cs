﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RpvStartBotService.Models;
using RpvStartBotService.Services;

namespace RpvStartBotService.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IBotServiceManagement _botService;               

        public HomeController(ILogger<HomeController> logger, IBotServiceManagement botService)
        {
            _logger = logger;            
            _botService = botService;
        }

        public async Task<IActionResult> Index()
        {
            var vkCount = await _botService.GetUniqueBotPropertieCount(BotType.Vk);
            var tlgCount = await _botService.GetUniqueBotPropertieCount(BotType.Telegram);
            var tlgAdCount = await _botService.GetUniqueBotPropertieCount(BotType.TelegramAdmin);

            var model = new InputOptionsModel(vkCount, tlgCount, tlgAdCount);
            return View(model);
        }  

        [HttpPost]
        public async Task<IActionResult> Action(InputOptionsModel model)
        {
            await _botService.CreateAndRunServices(model);            
            
            return Redirect("/Service/Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        
    }
}
