﻿using Microsoft.AspNetCore.Mvc;
using RpvStartBotService.Services;
using System;
using System.Linq;

namespace RpvStartBotService.Controllers
{
    public class ServiceController : Controller
    {
        private readonly IBotServiceManagement _botService;

        public ServiceController(IBotServiceManagement botService)
        {
            _botService = botService;
        }
        public IActionResult Index()
        {
            var services = _botService.GetServices();            
            return View(services);
        }
        public IActionResult IndexPartial()
        {
            var services = _botService.GetServices();
            return PartialView("_ServicePartial", services);
        }

        [HttpPost]
        public IActionResult ChangeState([FromBody]string serviceName)
        {
            var result = _botService.ChangeStateOfService(serviceName);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult DeleteService([FromBody]string serviceName)
        {
            _botService.DeleteService(serviceName);
            return Ok();
        }
    }
}
