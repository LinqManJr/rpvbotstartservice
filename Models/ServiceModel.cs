﻿using System.ServiceProcess;

namespace RpvStartBotService.Models
{
    public class ServiceModel
    {
        public string ServiceName { get; set; }
        public string DisplayName {get;set;}
        public ServiceControllerStatus Status { get; set; }
    }
}
