﻿using System.Collections.Generic;
using System.Linq;

namespace RpvStartBotService.Models
{
    public class InputOptionsModel
    {
        public InputOptionsModel()
        {

        }
        public InputOptionsModel(int vkcount = 1, int tlgcount = 1, int tlgAdmincount = 1)
        {
            var items = Enumerable.Repeat("", vkcount);
            var longItems = Enumerable.Repeat(default(long), vkcount);
            var itemsTlg = Enumerable.Repeat("", tlgcount);
            var itemsAdTlg = Enumerable.Repeat("", tlgAdmincount);

            VkToken = new List<string>(items);
            GroupID = new List<long>(longItems);
            Currency = new List<string>(items);

            TelegramToken = new List<string>(itemsAdTlg);
            TelegramAdminToken = new List<string>(itemsAdTlg);
            ClientTelegramToken = new List<string>(itemsTlg);
        }
        public string BaseApiAddress { get; set; } = "http://localhost:21021/";
       
        public string UserLogin { get; set; }
        public string UserPassword { get; set; }
        public string Tenant { get; set; } 

        public List<string> ClientTelegramToken { get; set; }


        public List<string> TelegramToken { get; set; }
        public List<string> TelegramAdminToken { get; set; }


        public List<string> VkToken { get; set; }
        public List<long> GroupID { get; set; }
        public List<string> Currency { get; set; }

    }
}
