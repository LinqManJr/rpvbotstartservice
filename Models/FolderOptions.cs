﻿namespace RpvStartBotService.Models
{
    public class FolderOptions
    {
        public string InputTelegramFolder { get; set; }
        public string InputVkFolder { get; set; }
        public string OutputFolder { get; set; }
        public string InputTelegramAdminFolder { get; set; }
    }
}
