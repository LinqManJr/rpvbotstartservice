﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace RpvStartBotService.Settings
{
    public class TelegramClientJson
    {
        public Logging Logging { get; set; }
        public BotSettings BotSettings { get; set; }
    }
    public class Logging
    {
        public LogLevel LogLevel { get; set; }
    }

    public class LogLevel
    {
        public string Default { get; set; }
        public string Microsoft { get; set; } 

        [JsonPropertyName("Microsoft.Hosting.Lifetime")]
        public string LifeTime { get; set; }
    }

    public class BotSettings
    {
        public string BaseApiAddress { get; set; }
        public List<User> Users { get; set; }
    }
    public class User
    {
        public string UserLogin { get; set; }
        public string UserPassword { get; set; }
        public string ClientName { get; set; }
        public string TelegramToken { get; set; }        
    }
}
