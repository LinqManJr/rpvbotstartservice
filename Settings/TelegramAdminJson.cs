﻿using System.Collections.Generic;

namespace RpvStartBotService.Settings
{
    public class TelegramAdminJson
    {
        public Logging Logging { get; set; }
        public BotSettingsAdmin BotSettings { get; set; }
    }

    public class BotSettingsAdmin
    {
        public string BaseApiAddress { get; set; }
        public List<UserAdmin> Users { get; set; }

    }
    public class UserAdmin : User
    {
        public string TelegramAdminToken { get; set; }
    }
}
