﻿using System.Collections.Generic;

namespace RpvStartBotService.Settings
{
    public class VkJson
    {
        public List<VkUser> Users { get; set; }
    }

    public class VkUser
    {
        public string Token { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string TenacyName { get; set; }
        public string Currency { get; set; }
        public string BaseUrl { get; set; }
        public long GroupId { get; set; }
    }
}
