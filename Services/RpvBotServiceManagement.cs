﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RpvStartBotService.Models;
using RpvStartBotService.Settings;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace RpvStartBotService.Services
{
    public class RpvBotServiceManagement : IBotServiceManagement
    {
        private readonly FolderOptions _folderOptions;        
        
        public RpvBotServiceManagement(IOptions<FolderOptions> folderOptions)
        {
            _folderOptions = folderOptions.Value;            
        }
               
        public async Task CreateAndRunServices(InputOptionsModel inputModel)
        {
            if (Directory.Exists(_folderOptions.InputTelegramFolder))
            {
                var settingsPath = await CopyServiceData(inputModel, BotType.Telegram);
                var exePath = Directory.GetFiles(Path.GetDirectoryName(settingsPath), "*.exe").FirstOrDefault();

                CreateAndRunService(exePath, string.Join(" ","Rpv Bot Service", "Telegram", inputModel.Tenant));
            }
            if (Directory.Exists(_folderOptions.InputVkFolder))
            {
                var settingsPath = await CopyServiceData(inputModel, BotType.Vk);
                var exePath = Directory.GetFiles(Path.GetDirectoryName(settingsPath), "*.exe").FirstOrDefault();

                CreateAndRunService(exePath, string.Join(" ", "Rpv Bot Service", "VK", inputModel.Tenant));
            }
            if (Directory.Exists(_folderOptions.InputTelegramAdminFolder))
            {
                var settingsPath = await CopyServiceData(inputModel, BotType.TelegramAdmin);
                var exePath = Directory.GetFiles(Path.GetDirectoryName(settingsPath), "*.exe").FirstOrDefault();

                CreateAndRunService(exePath, string.Join(" ", "Rpv Bot Service", "Telegram Admin", inputModel.Tenant));
            }
        }

        public IEnumerable<ServiceModel> GetServices()
        {
            return ServiceController.GetServices()
                .Where(x => x.ServiceName.StartsWith("Rpv"))                
                .Select(x => new ServiceModel{ ServiceName = x.ServiceName, DisplayName = x.DisplayName, Status = x.Status })
                .OrderBy(x => x.ServiceName.Split(' ').Last());
        }

        public async Task<int> GetUniqueBotPropertieCount(BotType botType)
        {
            
            if (botType == BotType.Vk)
            {
                var obj = JsonConvert.DeserializeObject<VkJson>(File.ReadAllText(Path.Combine(_folderOptions.InputVkFolder, "settings.json")));
                return obj.Users.Count;
            }
               
            else if(botType == BotType.Telegram)
                using (FileStream fs = File.OpenRead(Path.Combine(_folderOptions.InputTelegramFolder, "appsettings.json")))
                    return (await System.Text.Json.JsonSerializer.DeserializeAsync<TelegramClientJson>(fs)).BotSettings.Users.Count;
            else 
                using (FileStream fs = File.OpenRead(Path.Combine(_folderOptions.InputTelegramAdminFolder, "appsettings.json")))
                    return (await System.Text.Json.JsonSerializer.DeserializeAsync<TelegramAdminJson>(fs)).BotSettings.Users.Count;
        }

        public bool ChangeStateOfService(string serviceName)
        {
            var service = ServiceController.GetServices().FirstOrDefault(x => x.ServiceName == serviceName);
            if (service == null)
                return false;

            string args = "";            

            if (service.Status == ServiceControllerStatus.Running)            
                args = $"stop \"{serviceName}\"";                
            
            else if (service.Status == ServiceControllerStatus.Stopped)            
                args = $"start \"{serviceName}\"";

            ExecuteProcess("sc", args);
            return true;
        }

        public void DeleteService(string serviceName)
        {
            string deleteArgs = $"delete \"{serviceName}\"";
            ExecuteProcess("sc", deleteArgs);
        }



        private async Task<string> CopyServiceData(InputOptionsModel input, BotType botType)
        {
            string subFolder = botType.ToString();
            string outputSettingsPath = "";
            string jsonName;
            string settingsPath;
            string tenant = input.Tenant;

            if (botType == BotType.Telegram)
            {
                jsonName = "appsettings.json";
                settingsPath = Path.Combine(_folderOptions.InputTelegramFolder, jsonName);

                TelegramClientJson tlgJson;
                using (FileStream fs = File.OpenRead(settingsPath))
                    tlgJson = await System.Text.Json.JsonSerializer.DeserializeAsync<TelegramClientJson>(fs);

                tlgJson.UpdateTelegramSettings(input);
                tenant = string.IsNullOrWhiteSpace(tenant) ? tlgJson.BotSettings.Users.FirstOrDefault().ClientName : input.Tenant;

                Extensions.DirectoryCopy(_folderOptions.InputTelegramFolder, Path.Combine(_folderOptions.OutputFolder, tenant, subFolder), true);

                outputSettingsPath = Path.Combine(_folderOptions.OutputFolder, tenant, subFolder, jsonName);

                using (FileStream fs = File.Create(outputSettingsPath))
                    await System.Text.Json.JsonSerializer.SerializeAsync(fs, tlgJson);
            }
           
            if (botType == BotType.Vk)
            {
                jsonName = "settings.json";                
                settingsPath = Path.Combine(_folderOptions.InputVkFolder, jsonName);
                                
                VkJson vkJson = JsonConvert.DeserializeObject<VkJson>(File.ReadAllText(settingsPath));

                vkJson.UpdateVkSettings(input);

                //TODO: get tenant from vksettings if tenant is null
                Extensions.DirectoryCopy(_folderOptions.InputVkFolder, Path.Combine(_folderOptions.OutputFolder, tenant, subFolder), true);

                outputSettingsPath = Path.Combine(_folderOptions.OutputFolder, tenant, subFolder, jsonName);                                
                /*
                using (FileStream fs = File.Create(outputSettingsPath))
                {
                    JsonSerializerOptions opt = new JsonSerializerOptions() { WriteIndented = true };
                    await System.Text.Json.JsonSerializer.SerializeAsync(fs, vkJson, opt);
                }*/

                //without quotes if need
                
                var serializer = new Newtonsoft.Json.JsonSerializer();
                var stringWriter = new StringWriter();
                using (var writer = new JsonTextWriter(stringWriter))
                {
                    writer.QuoteName = false;
                    serializer.Serialize(writer, vkJson);
                }
                var json = stringWriter.ToString();
                await File.WriteAllTextAsync(outputSettingsPath, json);
            }

            if(botType == BotType.TelegramAdmin)
            {
                jsonName = "appsettings.json";
                settingsPath = Path.Combine(_folderOptions.InputTelegramAdminFolder, jsonName);

                TelegramAdminJson tlgJson;
                using (FileStream fs = File.OpenRead(settingsPath))
                    tlgJson = await System.Text.Json.JsonSerializer.DeserializeAsync<TelegramAdminJson>(fs);

                tlgJson.UpdateTelegramSettings(input);
                subFolder = "Admin";
                Extensions.DirectoryCopy(_folderOptions.InputTelegramAdminFolder, Path.Combine(_folderOptions.OutputFolder, tenant, subFolder), true);

                outputSettingsPath = Path.Combine(_folderOptions.OutputFolder, tenant, subFolder, jsonName);

                using (FileStream fs = File.Create(outputSettingsPath))
                    await System.Text.Json.JsonSerializer.SerializeAsync(fs, tlgJson);
            }
            
            return outputSettingsPath;
        }

        private void CreateAndRunService(string dirPathExe, string serviceName)
        {
            string createArgs = $"create \"{serviceName}\" binpath= \"{dirPathExe}\" displayname= \"{serviceName}\" start= auto";
            string optionsArgs = $"failure \"{serviceName}\" reset= 0 actions= restart/0";
            string startArgs = $"start \"{serviceName}\"";                        

            foreach (var args in new[] { createArgs, optionsArgs, startArgs })
            {
                ExecuteProcess("sc", args);                
            }
        }

        private void ExecuteProcess(string procName, string args)
        {
            var processinfo = new ProcessStartInfo(procName);
            processinfo.Verb = "runas";
            processinfo.UseShellExecute = true;
            processinfo.WindowStyle = ProcessWindowStyle.Hidden;
            processinfo.Arguments = args;
            using (var process = Process.Start(processinfo))
            {
                while (!process.HasExited)
                {
                    Thread.Sleep(300);
                }
            }
            
        }


    }
}
