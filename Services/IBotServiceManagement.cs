﻿using RpvStartBotService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RpvStartBotService.Services
{
    public interface IBotServiceManagement
    {
        Task<int> GetUniqueBotPropertieCount(BotType botType);
        Task CreateAndRunServices(InputOptionsModel inputModel);
        IEnumerable<ServiceModel> GetServices();
        bool ChangeStateOfService(string serviceName);
        void DeleteService(string serviceName);
    }
}
