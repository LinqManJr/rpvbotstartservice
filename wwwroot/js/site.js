﻿$(document).ready(function () {
    $("body").on("click", ".stateLink", function () {        
        var par = this.parentNode.parentNode;
        var tdChild = par.children[2];        
        let serviceName = tdChild.id;

        if (this.text == "Запустить")
            this.text = "Остановить";
        else
            this.text = "Запустить";

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Service/ChangeState",
            dataType: "json",
            data: JSON.stringify(serviceName),
            success: function (response) {
                if (response) {                    

                    if (tdChild.innerText == "Running")
                        tdChild.innerText = "Stopped";
                    else
                        tdChild.innerText = "Running";
                }                
            }
        });
    });

    $("body").on("click", ".removeLink", function () {
        var tr = this.parentNode.parentNode;
        var serviceName = tr.id.split('_')[1];        

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Service/DeleteService",
            dataType: "json",
            data: JSON.stringify(serviceName),
            success: function (response) {
                if (response) {
                    tr.remove();
                }
            }
        });
    });
    var id = setInterval(function () {
        $("#serviceResult").load('/Service/IndexPartial');
    }, 10000)
});